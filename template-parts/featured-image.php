<?php
/**
 * Displays the featured image
 *
 * @package WordPress
 * @subpackage Twenty_Twenty
 * @since 1.0.0
 */

if ( has_post_thumbnail() && ! post_password_required() ) {

	$featured_img = wp_get_attachment_url( get_post_thumbnail_id($post->ID) ); 
	$featured_img_classes = get_post_meta(get_the_ID(), 'meta-box-classes', true);
?>
<div class="cover-header-wrapper <?php echo $featured_img_classes; ?>">
	<div class="cover-header"
		<?php if( $featured_img ) : ?>
		style="background-image: url(<?php echo $featured_img; ?>);"
		<?php endif; ?>
			>
	</div>
	<div class="entry-header">
		<div>
			<?php echo strip_tags( $post->post_excerpt ); ?>
		</div>
	</div>

</div>
	<?php
}
